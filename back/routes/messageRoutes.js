const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
    '/',
    (req, res, next) => {
        res.data = MessageService.getAll();
        next();
    },
    responseMiddleware
);

router.get(
    '/:id',
    (req, res, next) => {
        const chosenMessage = MessageService.search({ id: req.params.id });

        if (chosenMessage) {
            res.data = chosenMessage;
        } else {
            const e = new Error('Cannot find message with such id');
            e.status = 404;
            res.err = e;
        }
        next();
    },
    responseMiddleware
);

router.post(
    '/',
    (req, res, next) => {
        if (req.body.text) {
            res.data = MessageService.create(req.body);
        } else {
            res.err = new Error('Validation error');
        }
        next();
    },
    responseMiddleware
);

router.put(
    '/:id',
    (req, res, next) => {
        if (req.body.text && req.body.id) {
            res.data = MessageService.update(req.params.id, req.body);
        } else {
            res.err = new Error('Validation error');
        }
        next();
    },
    responseMiddleware
);

router.delete(
    '/:id',
    (req, res, next) => {
        res.data = MessageService.delete(req.params.id);
        next();
    },
    responseMiddleware
);

router.post(
    '/:id/likes',
    (req, res, next) => {
        if (req.body.likerId) {
            res.data = MessageService.addLike(req.params.id, req.body.likerId);
        } else {
            res.err = new Error('Validation error');
        }
        next();
    },
    responseMiddleware
);

router.delete(
    '/:id/likes/:likerId',
    (req, res, next) => {
        res.data = MessageService.removeLike(req.params.id, req.params.likerId);
        next();
    },
    responseMiddleware
);

module.exports = router;
