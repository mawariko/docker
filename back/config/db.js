const dbPath = '/data/database.json';

// const dbPath = 'C:/Users/Maryna/Desktop/BSA-Lectures/Homework11_serverdocker/data/database.json';

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync(dbPath);

const dbAdapter = low(adapter);

const defaultDb = { users: [], messages: []};

dbAdapter.defaults(defaultDb).write();

exports.dbAdapter = dbAdapter;