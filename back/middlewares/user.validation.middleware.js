const {
    user
} = require('../models/user');
const UserService = require('../services/userService');

const roles = {
    admin: 'admin',
    user: 'user'
}

const patterns = {
    email: /^[^@\s]+@[^@\s\.]+\.[^@\.\s]+$/,
    notempty: /^\S+$/,
    password: /^.{3,}$/
};

const roleValidator = (value, message, res) => {
    if ((value != roles.admin ) && (value != roles.user)) {
            res.err = Error(message);
            return false;   
    } else {
        return true
    }
}

function validate(regex, value, message, res) {
    if (value !== undefined && regex.test(value)) {
        return true;
    } else {
        res.err = Error(message);
        return false;
    }
}

function blockFields(data, res) {
    let valid = true;
    Object.keys(data).forEach(key => {
        if (key === 'id' || user[key] === undefined) {
            valid = false;
            res.err = Error(`Not supported field: ${key}`);
        }
    });
    return valid;
}


const createUserValid = (req, res, next) => {
    let validCreate = true;

    const duplicateEmailUser = UserService.search({
        email: req.body.email
    });

    if (duplicateEmailUser) {
        validCreate = false;
        res.err = Error('User with such email already exists');
    }

    validCreate = validCreate &&
        validate(patterns.email, req.body.email, "Fill in a proper email", res) &&
        validate(patterns.notempty, req.body.name, "Fill in user's name", res) &&
        roleValidator(req.body.role, "Fill in user's role", res) &&
        blockFields(req.body, res);

    req.valid = validCreate;
    req.validatedBody = req.body;
    next();
}


const updateUserValid = (req, res, next) => {
    let validUpdate = true;

    if(!UserService.search({id: req.params.id})) {
        validUpdate = false;
        res.err = Error('Can not find user with such id');
    }

    if (validUpdate && req.body.name) {
        validUpdate = validate(patterns.notempty, req.body.name, "Fill in user's name", res);
    }
    if (validUpdate && req.body.email) {
        validUpdate = validate(patterns.email, req.body.email, "Fill in a proper email adress", res);
    }
    if (validUpdate && req.body.role) {
        validUpdate =  roleValidator(req.body.role, "Fill in user's role", res);
    }
    if (validUpdate) {
        validUpdate = blockFields(req.body, res);
    }
   
    req.valid = validUpdate;
    req.validatedBody = req.body;
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;