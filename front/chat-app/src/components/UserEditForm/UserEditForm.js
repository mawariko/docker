import React, {useState} from 'react'
import './UserEditForm.css'
import { connect } from 'react-redux'
import { editUser } from '../../actions'

const roles = {
    ADMIN: 'admin',
    USER: 'user',
}

function UserEditForm({ currentlyEditedUser, editUser }) {

    const [name, setName] = useState(currentlyEditedUser.name);
    const [email, setEmail] = useState(currentlyEditedUser.email);
    const [role, setRole] = useState(currentlyEditedUser.role);
    
    const submitHandler = () => {
        if(name && email && role) {
            editUser({id: currentlyEditedUser.id, name, email, role})
        }
    }

    return (
       <div className='user-edit-form'>
            <span>Name:</span><input value={name} onChange={(e) => setName(e.target.value)}></input>      
            <span>Email:</span><input value={email} onChange={(e) => setEmail(e.target.value)}></input>      
            <span>Role:</span><select onChange={(e) => setRole(e.target.value)}>
                <option value="admin" selected={role === roles.ADMIN}>Admin</option>
                <option value="user" selected={role === roles.USER}>User</option>
            </select>   
            <button className='save-btn' onClick={submitHandler}>SAVE</button>      
       </div>
    )
}

const mapDispatchToProps = {
    editUser
}

export default connect(null, mapDispatchToProps)(UserEditForm)