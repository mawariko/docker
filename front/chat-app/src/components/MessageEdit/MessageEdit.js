import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { updateMessage, cancelEditMessage } from '../../actions';
import Auth from '../Auth/Auth';
import './MessageEdit.css';

function MessageEdit({ currentlyEditedMessage, updateMessage, cancelEditMessage }) {
  const [text, setText] = useState('');

  const submitHandler = () => {
    if (text) {
      updateMessage({
        id: currentlyEditedMessage.id,
        text,
      });
      setText('');
    }
  };

  useEffect(() => {
    if (currentlyEditedMessage) {
      setText(currentlyEditedMessage.text);
    }
  }, [currentlyEditedMessage]);

  return (
    <>
      <Auth />
      <div className="modal-box">
        <div className="modal-header">Edit Message</div>
        <div className="modal-message">
          <textarea
            name="message-input"
            rows="5"
            cols="10"
            onChange={(e) => setText(e.target.value)}
            value={text}
          ></textarea>
        </div>
        <div className="modal-buttons">
          <button className="modal-ok-button" onClick={submitHandler}>
            Ok
          </button>
          <button className="modal-cancel-button" onClick={cancelEditMessage}>
            Cancel
          </button>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  currentlyEditedMessage: state.currentlyEditedMessage,
});

const mapDispatchToProps = {
  updateMessage,
  cancelEditMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEdit);
