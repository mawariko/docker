import React, { useEffect } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import User from '../Users/User'
import UserEditForm from '../UserEditForm/UserEditForm'
import './AdminEdit.css'
import { showUsersToEdit } from '../../actions'

function AdminEdit({users, currentlyEditedUser, showUsersToEdit}) {
    useEffect(()=>{
        showUsersToEdit();
    },[])
   
    console.log(users)
    console.log(currentlyEditedUser)
    return (
        <>
        <div className='admin-header'>
            <h2 className='heading'>Choose a user to edit</h2>
            <Link to="chat" className='chat-link' >Chat</Link>   
        </div>
        <div className='admin-edit-container'>
            <div className='users-container'> 
             {users.map((user, index) => <User userToShow={user} key={index} />)}
        </div>
      
 
        {currentlyEditedUser ? (
            <div className='users-edit-container'>
                <div className='user-edit-form'>
                    <UserEditForm currentlyEditedUser={currentlyEditedUser} key={currentlyEditedUser.id} /> 
                    
                </div>
            </div>
            ) : (
            ''
            )}   
        </div>
      
        </>
    )
}

const mapStateToProps = (state) => ({
    users: state.users,
    currentlyEditedUser: state.currentlyEditedUser
})

const mapDispatchToProps = {
    showUsersToEdit
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminEdit)

 // {users.map((user, index) => <User userToShow={user.user} key={index} />)}

