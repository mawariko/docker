import React from 'react';
import moment from 'moment';
import './ChatHeader.css';

export default function ChatHeader({ uniqueUsers, messageNumber, lastMessageDate }) {
  const lastMessageString = lastMessageDate
    ? (lastMessageDate.isAfter(moment().startOf('day'))
      ? lastMessageDate.format('h:mm A')
      : lastMessageDate.calendar())
    : null;

  return (
    <div className="chat-header">
      <p>*** ranters</p>
      <p>{uniqueUsers} participants</p>
      <p>{messageNumber} messages</p>
      {lastMessageString && <p>last message: {lastMessageString}</p>}
    </div>
  )
}
