import React from 'react'
import { connect } from 'react-redux'
import { toggleLike, deleteMessage, editMessage } from '../../actions'


import './Message.css'

function Message({
  id,
  text,
  user,
  avatar,
  updatedAt,
  createdAt,
  isMine,
  likers,
  isMyLike,
  toggleLike,
  deleteMessage,
  editMessage,
}) {
  return (
    <div className={`message-box${isMine ? ' my-message' : ''}`}>
      <div className="message-details">
        <p>{user}</p>
        <p>{createdAt.format('h:mm A')}</p>
        {updatedAt && (
          <span className="edited">&#9998;{updatedAt.format('h:mm A')}</span>
        )}
      </div>
      <div className="message-content">
        {!isMine && <img src={avatar} alt="user" />}
        <p>{text}</p>
        {!isMine && (
          <>
            {likers.length > 0 && <span>{likers.length}</span>}
            <span
              onClick={() => toggleLike(id)}
              className={`icon message-heart ${isMyLike ? 'my-like' : ''}`}
            >
              &#9825;
            </span>
          </>
        )}
        {isMine && (
          <div className="my-icons">
            <span onClick={() => deleteMessage(id)} className="icon message-trash">
              &#128465;
            </span>
            <span
              onClick={() => editMessage(id)}
              className="icon message-edit"
            >
              &#9998;
            </span>
          </div>
        )}
      </div>
    </div>
  )
}

const mapDispatchToProps = {
  toggleLike,
  deleteMessage,
  editMessage
}

export default connect(null, mapDispatchToProps)(Message)