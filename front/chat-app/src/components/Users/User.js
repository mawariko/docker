import React, { useDebugValue } from 'react'
import UserEditForm from '../UserEditForm/UserEditForm';
import './User.css'
import { connect } from 'react-redux'
import { setCurrentlyEditedUser } from '../../actions'

function User({userToShow, setCurrentlyEditedUser}) {
    const handleChooseUser = () => {
        console.log(userToShow)
        setCurrentlyEditedUser(userToShow)
        console.log('action fired')
    }
    return (
        <div className='user-container'>
            <div className='user-item'>
                {userToShow.name}
            </div>
            <button className='edit-btn' onClick={handleChooseUser}>EDIT</button>
        </div>
    )
}

const mapDispatchToProps = {
    setCurrentlyEditedUser
}
export default connect(null, mapDispatchToProps)(User)