import {
  SHOW_SPINNER,
  HIDE_SPINNER,
  TOGGLE_LIKE,
  TOGGLE_LIKE_SUCCESS,
  CREATE_MESSAGE,
  CREATE_MESSAGE_SUCCESS,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  EDIT_MESSAGE,
  EDIT_MESSAGE_SUCCESS,
  UPDATE_MESSAGE,
  UPDATE_MESSAGE_SUCCESS,
  CANCEL_EDIT_MESSAGE,
  CANCEL_EDIT_MESSAGE_SUCCESS,
  LOGIN,
  LOGIN_SUCCESS,
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
  SHOW_USERS_TO_EDIT,
  SHOW_USERS_TO_EDIT_SUCCESS,
  EDIT_USER,
  EDIT_USER_SUCCESS,
  CANCEL_EDIT_USER,
  CANCEL_EDIT_USER_SUCCESS,
  SET_CURRENTLY_EDITED_USER
} from './actionTypes';

export const showSpinner = () => ({
  type: SHOW_SPINNER,
});

export const hideSpinner = () => ({
  type: HIDE_SPINNER,
});

export const toggleLike = (id) => ({
  type: TOGGLE_LIKE,
  id,
});

export const toggleLikeSuccess = (id, likers, isMyLike) => ({
  type: TOGGLE_LIKE_SUCCESS,
  payload: { id, likers, isMyLike },
});

export const fetchMessages = () => ({
  type: FETCH_MESSAGES,
});

export const fetchMessagesSuccess = (messages) => ({
  type: FETCH_MESSAGES_SUCCESS,
  messages,
});

export const createMessage = (text) => ({
  type: CREATE_MESSAGE,
  text,
});

export const createMessageSuccess = (message) => ({
  type: CREATE_MESSAGE_SUCCESS,
  message,
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  id,
});

export const deleteMessageSuccess = (id) => ({
  type: DELETE_MESSAGE_SUCCESS,
  id,
});

export const editMessage = (id) => ({
  type: EDIT_MESSAGE,
  id,
});

export const editMessageSuccess = (message) => ({
  type: EDIT_MESSAGE_SUCCESS,
  message,
});

export const updateMessage = (message) => ({
  type: UPDATE_MESSAGE,
  message,
});

export const updateMessageSuccess = (message) => ({
  type: UPDATE_MESSAGE_SUCCESS,
  message,
});

export const cancelEditMessage = () => ({
  type: CANCEL_EDIT_MESSAGE,
});

export const cancelEditMessageSuccess = () => ({
  type: CANCEL_EDIT_MESSAGE_SUCCESS,
});

export const login = (email, password) => ({
  type: LOGIN,
  payload: { email, password },
});

export const loginSuccess = (isLoggedIn, userId, userRole) => ({
  type: LOGIN_SUCCESS,
  payload: { isLoggedIn, userId, userRole },
});

export const showUsersToEdit = () => ({
  type: SHOW_USERS_TO_EDIT,
});

export const showUsersToEditSuccess = (users) => ({
  type: SHOW_USERS_TO_EDIT_SUCCESS,
  users,
});

export const editUser = (editedUser) => ({
  type: EDIT_USER,
  payload: editedUser
})

export const editUserSuccess = (editedUser) => ({
  type: EDIT_USER_SUCCESS,
  payload: editedUser
})

export const setCurrentlyEditedUser = (user) => ({
  type: SET_CURRENTLY_EDITED_USER,
  payload: user
})
