import { all } from 'redux-saga/effects';
import authSagas from './authSagas';
import usersSagas from './usersSagas';
import messagesSagas from './messagesSagas';

export default function* rootSaga() {
    yield all([
        authSagas(),
        usersSagas(),
        messagesSagas()
    ])
};