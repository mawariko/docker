import { all, takeEvery, call, put } from 'redux-saga/effects';
import history from './../history';
import { login as serviceLogin } from '../services/domainRequest/auth';
import { LOGIN } from '../actions/actionTypes';
import { showSpinner, hideSpinner, loginSuccess } from '../actions';
import userRoles from '../constants/userRoles';

export default function* authSagas() {
  yield all([watchLogin()]);
}

function* watchLogin() {
  yield takeEvery(LOGIN, login);
}

function* login(action) {
  try {
    yield put(showSpinner());
    const data = yield call(serviceLogin, action.payload);
    if(data && !data.error) {
      yield put(hideSpinner());
      yield put(loginSuccess(true, data.id, data.role));
      yield call(
        [history, history.push],
        data.role === userRoles.ADMIN ? '/admin' : '/chat'
      );
    }
  } catch (error) {
    console.log(error.message);
  }
  yield put(hideSpinner());
}
