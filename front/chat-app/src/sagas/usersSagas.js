import { all, takeEvery, call, put, select } from 'redux-saga/effects';
import history from './../history';
import {
    getUsers,
    updateUser
  } from '../services/domainRequest/user';
  import {
    SHOW_USERS_TO_EDIT,
    EDIT_USER,
    EDIT_USER_SUCCESS,
    CANCEL_EDIT_USER,
    CANCEL_EDIT_USER_SUCCESS
  } from '../actions/actionTypes';
  import {
    showSpinner,
    hideSpinner,
    showUsersToEditSuccess,
    editUserSuccess
  } from '../actions';
  

export default function* usersSagas() {
    yield all([
        watchShowUsersToEdit(),
        watchEditUser()
    ]);
   
}

function* watchShowUsersToEdit() {
    yield takeEvery(SHOW_USERS_TO_EDIT,showUsersToEdit)
}

function* watchEditUser() {
    yield takeEvery(EDIT_USER, editUser)
}

function* showUsersToEdit(){
    yield put(showSpinner())
    try {
        const users = yield call(getUsers);
        yield put(showUsersToEditSuccess(users))
    } catch(error) {
        console.log(error.message);
    }
    yield put(hideSpinner());
}

function* editUser(action) {
    yield put(showSpinner());
    try {
        const updatedUser = yield call(updateUser, action.payload.id, {
            name: action.payload.name,
            email: action.payload.email,
            role: action.payload.role
        });
        yield put(editUserSuccess(updatedUser))
    } catch (error) {
        console.log(error.message)
    }
    yield put(hideSpinner());
}