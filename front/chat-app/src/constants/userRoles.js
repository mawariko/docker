const userRoles = {
  USER: 'user',
  ADMIN: 'admin',
};

export default userRoles;
