import {
    get,
    post,
    put,
    deleteReq
} from '../requestHelper';

export const getUsers = async () => {
    return await get('users');
};

export const getUser = async (id) => {
    return await get('users', id);
};

export const updateUser = async (id, body) => {
    return await put('users', id, body);
};