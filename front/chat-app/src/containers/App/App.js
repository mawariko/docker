import React from 'react';
import { connect } from 'react-redux';
import { Router, Switch, Route } from 'react-router-dom';
import history from '../../history';
import Chat from '../../containers/Chat/Chat';
import MessageEdit from '../../components/MessageEdit/MessageEdit';
import LogIn from '../../components/LogIn/LogIn';
import AdminEdit from '../../components/AdminEdit/AdminEdit';
import Spinner from '../../components/Spinner/Spinner';

function App({ showSpinner }) {
  return (
    <>
      {showSpinner ? <Spinner /> : ''}
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={LogIn} />
          <Route exact path="/chat" component={Chat} />
          <Route exact path="/chat/edit" component={MessageEdit} />
          <Route exact path="/admin" component={AdminEdit} />
        </Switch>
      </Router>
    </>
  );
}

const mapStateToProps = (state) => ({
  showSpinner: state.showSpinner,
});

export default connect(mapStateToProps)(App);
