FROM node:alpine as build

WORKDIR /app

COPY ./front/chat-app .

RUN npm install 
RUN npm install react-scripts@3.4.1 -g 

RUN npm run build

FROM nginx

COPY --from=build /app/build /usr/share/nginx/html
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf