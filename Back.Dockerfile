FROM node:alpine

WORKDIR /data
COPY ./data/database.json .

WORKDIR /app
COPY ./back .

RUN npm install 

CMD [ "npm", "run", "start" ]